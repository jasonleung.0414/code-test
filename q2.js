module.exports = (input) => {
  if (!Array.isArray(input)) {
    throw new TypeError('Input must be an array.');
  }

  const [size, ...items] = input;
  if (typeof size !== 'number') {
    throw new TypeError('Size must be a number.');
  }

  if (size < 0 || size > items.length) {
    throw new RangeError(`Range of size must be >0 and <=${items.length}.`);
  }

  const slicedItems = items.slice(0, size);

  if (slicedItems.length <= 3) {
    return slicedItems;
  }

  return [
    slicedItems[0],
    ...slicedItems.slice(1, -1).reverse(),
    slicedItems[slicedItems.length - 1]
  ];
}
