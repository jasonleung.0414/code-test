## How to run

### Q1
```bash
node -e "const ans = require('./q1')([1, 3, 'a', 'b', 4, 7]); console.log(ans);"
```

### Q2
```bash
node -e "const ans = require('./q2')([5, 'a', 'b', 'c', 'd', 'e', 'f', 'g']); console.log(ans);"
```
