module.exports = (input) => {
  if (!Array.isArray(input)) {
    throw new TypeError('Input must be an array.');
  }

  return input.filter(Number.isFinite);
}
